# libxz
libxz system operation media for image, audio ,and video

# usage

> Usage
```
const libxz = require('libxz')
```

> For npm
```
npm install libxz
```